#!/usr/bin/env bash

# SPDX-License-Identifier: BSD-3-Clause

set -x

MORELLO_HOME="$(pwd)"
MORELLO_GEN="$MORELLO_HOME/out/generated"

build_morello_image() {
	local PLATFORM=$1
	# Note: The debian image must be placed first due to grub config
	local PARTITIONS=(
		"::$MORELLO_GEN/morello-debian.img"
		"::$MORELLO_GEN/morello-pcuabi-env.img"
	)


	"$MORELLO_HOME/scripts/tools/mk-part-fat" \
		-o "$MORELLO_GEN/bsp/$PLATFORM/morello.esp.img" \
		-s "AUTO" \
		-l "ESP" \
		"$MORELLO_GEN/bsp/firmware/$PLATFORM/grub.efi"		"/EFI/BOOT/BOOTAA64.EFI" \
		"$MORELLO_GEN/bsp/firmware/$PLATFORM/grub_$PLATFORM.cfg"	"/grub/grub.cfg" \
		"$MORELLO_GEN/dts/arm/morello-$PLATFORM.dtb"		"/morello.dtb" \
		"$MORELLO_GEN/Image"					"/Image"

	"$MORELLO_HOME/scripts/tools/mk-disk-gpt" \
		-o "$MORELLO_HOME/out/morello-$PLATFORM.img" \
		"ESP::$MORELLO_GEN/bsp/$PLATFORM/morello.esp.img" \
		"${PARTITIONS[@]}"
}

PLATFORMS=( soc fvp )

for p in ${PLATFORMS[@]}
do
	build_morello_image $p
done
