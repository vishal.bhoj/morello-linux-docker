# SPDX-License-Identifier: BSD-3-Clause

SHELL:=./scripts/shell-wrapper.sh

.SILENT: make_image image clean

make_image:
	./create_image.sh

all: make_image

image:
	./scripts/build-morello-image.sh

clean_docker:
	docker image prune
	docker image rm morello-debian:bullseye debian:bullseye

log:
	./create_image.sh |& tee log.txt

tags:
	@./scripts/tag-morello-image.sh out/

clean:
	touch log.txt
	rm log.txt
	rm -fr out/
