#!/usr/bin/env bash

# SPDX-License-Identifier: BSD-3-Clause

set -x

export LANG=C

/debootstrap/debootstrap --second-stage

apt-get update
apt-get upgrade

# Enable multi-user runlevel (3)
systemctl set-default multi-user.target
export RUNLEVEL=1

PACKAGES=(
	build-essential
	device-tree-compiler
	cmake
	make
	binutils
	curl
	ca-certificates
	gnupg
	lsb-release
	openssh-server
	git
	git-core
	python3
	fakeroot
	strace
	file
	ncurses-dev
	xz-utils
	bc
	flex
	bison
	wget
	rsync
	libelf-dev
	libssl-dev
	libncurses5
	libncursesw5
	libncurses5-dev
	libncursesw5-dev
)

# Install dependencies
apt-get install -y "${PACKAGES[@]}"

echo 'root:morello' | chpasswd

# Enable /dev/shm (required to build EDK2)
echo -e "none /dev/shm tmpfs rw,nosuid,nodev,noexec 0 0" >> /etc/fstab
mount /dev/shm

# Enable network
systemctl enable systemd-networkd
echo -e "[Match]\nName=e*\n[Network]\nDHCP=yes" | tee /etc/systemd/network/dhcp.network >/dev/null
echo morello > /etc/hostname
echo 127.0.0.1 localhost > /etc/hosts

# Allow root access to ssh
sed -i 's|#PermitRootLogin prohibit-password|PermitRootLogin yes|g' /etc/ssh/sshd_config

# Generate locale
apt-get install locales -y
locale-gen "en_US.UTF-8"

# Install docker inside the morello-debian image
curl -sSL https://get.docker.com | sh

# Build morello-pcuabi-env project
export MORELLO_HOME=/morello-pcuabi-env/morello
export MORELLO_PROJECTS=$MORELLO_HOME/projects

# Make sure we are in root (/)
cd /

# Clone morello-pcuabi-env
git clone https://git.morello-project.org/morello/morello-pcuabi-env.git -b morello/mainline

cd $MORELLO_HOME

# Build morello-pcuabi-env
source $MORELLO_HOME/env/morello-pcuabi-env
$MORELLO_HOME/scripts/build-all.sh \
	--aarch64 \
	--kselftest \
	--c-apps \
	--rootfs \
	--docker \
	--install
source $MORELLO_HOME/env/morello-pcuabi-env-restore

# Install Entropy generators
apt-get install rng-tools haveged jitterentropy-rngd -y

# Create /etc/resolv.conf
cat > /etc/resolv.conf << EOF
nameserver 8.8.8.8
nameserver 8.8.4.4
EOF

# Create local service for the final system configuration
cat > /etc/systemd/system/morello-config.service << EOF
[Unit]
Description=/etc/morello.config
ConditionPathExists=/etc/morello.config

[Service]
Type=forking
ExecStart=/etc/morello.config start
TimeoutSec=0
StandardOutput=tty
RemainAfterExit=yes
SysVStartPriority=99

[Install]
WantedBy=multi-user.target
EOF

cat > /etc/morello.config << EOF
#!/bin/sh -e
#
# morello.config
#
# This script is executed at the end of each multiuser runlevel.

/usr/local/bin/morello.config.sh

# Make sure that the script will "exit 0" on success.
exit 0
EOF

chmod +x /etc/morello.config

cat > /usr/local/bin/morello.config.sh << EOF
#!/usr/bin/env bash

MORELLO_BLUE='\\033[0;34m' # Blue
MORELLO_NC='\\033[0m' # No Color

MORELLO_CONFIG="/var/log/morello.config.log"
if [[ ! -f \$MORELLO_CONFIG ]]; then
	echo -e "[\${MORELLO_BLUE}MORELLO\${MORELLO_NC}]: Configure the system..."

	# Setup firewall
	update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
	update-alternatives --set iptables /usr/sbin/iptables-legacy

	# Restart Docker
	systemctl daemon-reload
	systemctl start docker

	touch "\$MORELLO_CONFIG"
else
	echo -e "[\${MORELLO_BLUE}MORELLO\${MORELLO_NC}]: Welcome to Morello!"
fi
EOF

chmod +x /usr/local/bin/morello.config.sh

# Enable service
systemctl enable morello-config
