# Morello Docker to bootstrap a Linux Environment
This project provides a docker configuration file that is suitable to build a Debian (AArch64) based rootfs usable on Morello SoCs or Morello FVP.

## Latest Images
* [Latest Image for Morello SoC](https://git.morello-project.org/morello/morello-linux-docker/-/jobs/artifacts/morello/mainline/raw/morello-soc.tar.xz?job=build-morello-linux-docker)
* [Latest Image for Morello FVP](https://git.morello-project.org/morello/morello-linux-docker/-/jobs/artifacts/morello/mainline/raw/morello-fvp.tar.xz?job=build-morello-linux-docker)
* [Image Registry](https://git.morello-project.org/morello/morello-linux-docker/-/packages)

### Decompress an Image
To decompress and verify a downloaded image (FVP or SoC) follow the steps below:
```
$ tar -xJf ~/<PATH>/morello-soc.tar.xz
$ ls morello-soc/*
morello-soc/morello-soc.img  morello-soc/version.txt
$ cat morello-soc/version.txt
version: <date>
```

# Morello Docker Deployment

## Pre-requisites
* A Debian based environment with network access
* [Docker](https://docs.docker.com/get-docker/)

### Setting up the build environment
* `$ apt install qemu-user-static tar xz-utils dosfstools fdisk make`

`Note`: The image generation in a third party provided docker container requires the installation of the tools above in the host system as well.

### Setting up Docker
Install docker:
```
curl -sSL https://get.docker.com | sh
```

Install docker-compose:

Latest: v2.15.1

Installation command:
```
$ sudo curl -L "https://github.com/docker/compose/releases/download/v2.15.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

Provide correct permissions to docker compose:
```
$ sudo chmod +x /usr/local/bin/docker-compose
```

Test docker-compose:
```
$ docker-compose --version
```

## Building
### Quick build:
```
$ make
```

### make options
* `make` / `make all` / `make make_image`:
The default target. It ultimately builds a GPT bootable disk image and firmware needed to boot it for the Morello FVP and Morello SoC.
* `make image`:
Only creates the GPT bootable disk image using the two partition images: `morello-debian.img`, `morello-pcuabi-env.img`, and the GRUB, TF-A, and Linux Kernel images.
* `make tags`:
Tags the current images updating the name to include the current date. `Note`: tagged images are preserved after a `make clean`.
* `make clean`:
Removes the `out` directory
* `make clean-docker`:
Removes the docker images: `morello-debian:bullseye`, `debian:bullseye`

### Output
All of the output artifacts are available in the `out/generated` directory:
* `morello-debian.img`: A partition image containing Morello Debian generated using `debootstrap`. It comes with Docker installed, all the necessary tools for software development, and an already built version of the [morello-pcuabi-env](https://gitlab.oss.arm.com/engineering/linux-arm/morello-pcuabi-env) repository.
* `morello-pcuabi-env.img`: A partition image containing a Morello Purecap Busybox environment for development.
* `Image`: The generated Morello Linux Kernel Image
* `bsp/fvp` and `bsp/soc`: Software components specific either to Morello FVP or Morello SoC
  - `morello.img`: GPT bootable disk image
  - `intermediates`:
    - `morello.esp.img`: EFI System partition image file formatted as vfat. Created based on an ARM64 GRUB EFI binary, ARM64 DTB, and Morello Linux Kernel Image.
    - `grub`: See [GRUB](https://savannah.gnu.org/projects/grub/) and [morello-pcuabi-env build-grub.sh](https://gitlab.oss.arm.com/engineering/linux-arm/morello-pcuabi-env/-/blob/morello/mainline/morello/scripts/build-grub.sh) for details on its build

All of the output images are available in the `out` directory:
* `morello-fvp.img`: Base image for the Morello FVP.
* `morello-soc.img`: Base image for the Morello SoC.

### Step by step build explanation
1. `Makefile`: `make` calls the `create_image.sh` script in a new shell.

1. `create_image.sh`: Performs all the necessary operations to create the final bootable disk image. Each script called as part of executing this script is detailed in their own sections.
   - create a Docker Debian image with all the required dependencies and the contents of `context`
   - run the Docker Debian image as a Docker container to execute `make_image.sh`
   - once the container finished executing, copy the output artifacts back to the host: `morello-debian.img`, `morello-pcuabi-env.img`, `morello/bsp`, and `Image`.
   - remove the Docker container
   - call the `build-morello-image.sh` script to generate the bootable GPT disk image from the previously generated artifacts.

1. `make_image.sh`: executed as part of the Docker container
   - `debootstrap` `morello-debian.img` in 2 stages
   - in stage 1, build the necessary `firmware` and the Morello Linux Kernel Image
   - in stage 2, setup Morello Debian, install Docker, and build the `morello-pcuabi-env.img`.

1. `build-morello-image.sh`:
   - create `morello.esp.img` EFI System partition based on firmware and Morello Linux Kernel Image.
   - create `morello.img` GPT bootable disk image from `morello.esp.img` and the `morello-debian.img`, `morello-pcuabi-env.img` partitions

`Note`: Make sure that at least one loop device is available before proceeding:
```
$ sudo losetup -f
```
