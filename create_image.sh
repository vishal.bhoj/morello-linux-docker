#!/usr/bin/env bash

# SPDX-License-Identifier: BSD-3-Clause

set -x

MORELLO_HOME="$(pwd)"
MORELLO_OUT=./../out/generated

# Create the Docker image
__image_name='morello-debian'
__image_tag='bullseye'

cd ./context

# --network host required when running with VPN
docker build --rm --network host  --tag ${__image_name}:${__image_tag} -f ./../config/Dockerfile.bullseye .

# The new docker image will contain a script to debootstrap Debian within
# the container ( @see Dockerfile )
# The container iteslf will be disposed once done
__container_name=morello_debian
docker run --name ${__container_name} --network host --device=$(losetup -f) --cap-add SYS_ADMIN --security-opt "apparmor=unconfined" ${__image_name}:${__image_tag} /bin/bash /morello_sw/scripts/make_image.sh

mkdir -p $MORELLO_OUT

docker cp ${__container_name}:/morello_sw/morello-debian.img $MORELLO_OUT
docker cp ${__container_name}:/morello_sw/stage2/morello-pcuabi-env/morello/morello-pcuabi-env/morello-pcuabi-env.img $MORELLO_OUT
docker cp -a ${__container_name}:/morello_sw/stage1/morello-pcuabi-env/morello/bsp $MORELLO_OUT
docker cp -a ${__container_name}:/morello_sw/stage1/morello-pcuabi-env/morello/linux-out/arch/arm64/boot/dts $MORELLO_OUT
docker cp ${__container_name}:/morello_sw/stage1/morello-pcuabi-env/morello/linux-out/arch/arm64/boot/Image $MORELLO_OUT
docker rm -v ${__container_name}

echo 'DONE: morello-debian.img should be ready'

echo "Build Morello Images"
cd ${MORELLO_HOME}
${MORELLO_HOME}/scripts/build-morello-image.sh

echo 'Docker image morello-bullseye has not been removed. Run: make clean_docker to get that done'
